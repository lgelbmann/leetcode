LeetCode submissions
====================

Solutions in Go to some of the [LeetCode programming
problems](https://leetcode.com/problemset/all/).

How to run
----------

Run `go test ./... -bench . -benchtime 0.1s -benchmem` to execute all
tests and benchmarks.
