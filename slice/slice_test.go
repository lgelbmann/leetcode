package slice_test

import (
	"testing"

	"gitlab.com/lgelbmann/leetcode/slice"
)

func TestIntsEqual(t *testing.T) {
	type pair struct {
		a, b []int
	}
	nums := []int{6, 7, 8, 9}

	equalPairs := []pair{
		{nums, nums},
		{nums[:2], []int{6, 7}},
		{nums[:0], nums[2:2]},
		{nums[:0], nil},
		{nil, nums[:0]},
		{nil, nil},
	}
	for _, args := range equalPairs {
		if !slice.IntsEqual(args.a, args.b) {
			t.Errorf("%#v and %#v should compare equal", args.a, args.b)
		}
	}

	unequalPairs := []pair{
		{nums[:3], nums},
		{nums, nums[:3]},
		{nums[:3], []int{6, 8, 7}},
		{nums, nil},
		{nil, nums},
	}
	for _, args := range unequalPairs {
		if slice.IntsEqual(args.a, args.b) {
			t.Errorf("%#v and %#v shouldn't compare equal", args.a, args.b)
		}
	}
}

func TestSortedInts(t *testing.T) {
	cases := []struct {
		nums []int
		want []int
	}{
		{nil, []int{}},
		{[]int{}, []int{}},
		{[]int{7}, []int{7}},
		{[]int{4, 2, 4, 3}, []int{2, 3, 4, 4}},
	}
	for _, c := range cases {
		got := slice.SortedInts(c.nums)
		if !slice.IntsEqual(got, c.want) {
			t.Errorf("SortedInts(%#v) = %v, want %v", c.nums, got, c.want)
		} else if len(c.nums) > 0 && len(got) > 0 && &c.nums[0] == &got[0] {
			t.Errorf("SortedInts(%#v) should return a copy", c.nums)
		}
	}
}
