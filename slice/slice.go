// Package slice provides tools for working with slices.
package slice

import "sort"

// IntsEqual returns true if the two slices contain the same elements in the
// same order.
func IntsEqual(a, b []int) bool {
	if len(a) != len(b) {
		return false
	}
	if len(a) == 0 || &a[0] == &b[0] {
		return true
	}
	for i := range a {
		if a[i] != b[i] {
			return false
		}
	}
	return true
}

// SortedInts returns a sorted copy of a slice of ints.
func SortedInts(values []int) []int {
	newValues := make([]int, len(values))
	copy(newValues, values)
	sort.Ints(newValues)
	return newValues
}
