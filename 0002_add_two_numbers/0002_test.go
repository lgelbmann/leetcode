package p0002_add_two_numbers

import (
	"math/rand"
	"reflect"
	"runtime"
	"strconv"
	"strings"
	"testing"
)

func TestAddTwoNumbers(t *testing.T) {
	cases := []struct {
		a, b *ListNode
		want *ListNode
	}{
		{
			&ListNode{3, &ListNode{3, &ListNode{5, nil}}},
			&ListNode{7, &ListNode{8, &ListNode{9, nil}}},
			&ListNode{0, &ListNode{2, &ListNode{5, &ListNode{1, nil}}}},
		},
		{
			&ListNode{9, &ListNode{9, &ListNode{9, nil}}},
			&ListNode{3, nil},
			&ListNode{2, &ListNode{0, &ListNode{0, &ListNode{1, nil}}}},
		},
		{
			&ListNode{3, nil},
			&ListNode{1, &ListNode{2, &ListNode{3, nil}}},
			&ListNode{4, &ListNode{2, &ListNode{3, nil}}},
		},
		{
			&ListNode{0, nil},
			&ListNode{0, nil},
			&ListNode{0, nil},
		},
	}
	for _, c := range cases {
		got := addTwoNumbers(c.a, c.b)
		if !reflect.DeepEqual(got, c.want) {
			t.Errorf(
				"addTwoNumbers(%v, %v) = %v, want %v", (*listView)(c.a),
				(*listView)(c.b), (*listView)(got), (*listView)(c.want),
			)
		}
	}
}

func Benchmark(b *testing.B) {
	random := rand.New(rand.NewSource(1))
	largeA := largeNumber(random)
	largeB := largeNumber(random)
	cases := []struct {
		name string
		a, b *ListNode
	}{
		{"WorstCase", largeA, largeB},
		{"ImbalancedCase", &ListNode{9, nil}, largeB},
	}
	for _, c := range cases {
		b.Run(c.name, func(b *testing.B) {
			for i := 0; i < b.N; i++ {
				runtime.KeepAlive(addTwoNumbers(c.a, c.b))
			}
		})
	}
}

func largeNumber(random *rand.Rand) *ListNode {
	number := &ListNode{9, nil}
	for i := 0; i < 99; i++ {
		number = &ListNode{random.Intn(10), number}
	}
	return number
}

type listView ListNode

func (v *listView) String() string {
	var strs []string
	for ; v != nil; v = (*listView)(v.Next) {
		strs = append(strs, "&{"+strconv.Itoa(v.Val)+" ")
	}
	start := strings.Join(strs, "")
	end := strings.Repeat("}", len(strs))
	return start + "<nil>" + end
}
