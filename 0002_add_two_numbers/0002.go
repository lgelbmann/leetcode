package p0002_add_two_numbers

type ListNode struct {
	Val  int
	Next *ListNode
}

func addTwoNumbers(a, b *ListNode) *ListNode {
	maxResultDigits := 101
	digits := make([]uint8, 0, maxResultDigits)
	var sum uint8 = 0 // Stores the digit-wise sum as well as the carry digit.
	for {
		if a != nil {
			sum += uint8(a.Val)
			a = a.Next
		}
		if b != nil {
			sum += uint8(b.Val)
			b = b.Next
		}
		digits = append(digits, sum%10)
		sum /= 10
		if a == nil && b == nil && sum == 0 {
			break
		}
	}

	// Save time by allocating all nodes at once.
	nodes := make([]ListNode, len(digits))
	for i, digit := range digits {
		nodes[i].Val = int(digit)
		if i > 0 {
			nodes[i-1].Next = &nodes[i]
		}
	}
	return &nodes[0]
}
