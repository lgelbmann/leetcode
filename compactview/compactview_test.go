package compactview_test

import (
	"testing"

	"gitlab.com/lgelbmann/leetcode/compactview"
)

func TestIntsString(t *testing.T) {
	nums := compactview.Ints{5, -1, 0, 10, 3, 4, 5, 6, 7}
	cases := []struct {
		values compactview.Ints
		want   string
	}{
		{nil, "[]"},
		{nums[:0], "[]"},
		{nums[:1], "[5]"},
		{nums[:7], "[5 -1 0 10 3 4 5]"},
		{nums[:8], "[5 -1 0 ... 4 5 6]"},
		{nums, "[5 -1 0 ... 5 6 7]"},
	}
	for _, c := range cases {
		got := c.values.String()
		if got != c.want {
			t.Errorf("%#v.String() = %q, want %q", c.values, got, c.want)
		}
	}
}
