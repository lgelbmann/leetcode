// Package compactview provides compact string representations of values.
package compactview

import (
	"strconv"
	"strings"
)

const viewSize = 7

// An Ints value represents a slice of ints. It provides a compact view of its
// contents via the String method.
type Ints []int

// String returns a compact representation of the slice with only the beginning
// and end shown, e.g. "[1 2 3 ... 998 999 1000]".
func (values Ints) String() string {
	var strs []string
	if len(values) > viewSize {
		strs = make([]string, viewSize)
		startSize := viewSize / 2
		for i, v := range values[:startSize] {
			strs[i] = strconv.Itoa(v)
		}
		strs[startSize] = "..."
		endSize := viewSize - startSize - 1
		for i, v := range values[len(values)-endSize:] {
			strs[startSize+1+i] = strconv.Itoa(v)
		}
	} else {
		strs = make([]string, len(values))
		for i, v := range values {
			strs[i] = strconv.Itoa(v)
		}
	}
	return "[" + strings.Join(strs, " ") + "]"
}
