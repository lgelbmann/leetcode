package p0001_two_sum

import (
	"runtime"
	"testing"

	"gitlab.com/lgelbmann/leetcode/compactview"
	"gitlab.com/lgelbmann/leetcode/slice"
)

type testCase struct {
	nums   []int
	target int
	want   []int
}

func TestTwoSum(t *testing.T) {
	cases := []testCase{
		{[]int{2, 12, -7}, -5, []int{0, 2}},
		{[]int{1e9, -1e9, 5e8, 5e8}, 1e9, []int{2, 3}},
		worstCase(),
	}
	for _, c := range cases {
		got := twoSum(c.nums, c.target)
		if !slice.IntsEqual(slice.SortedInts(got), c.want) {
			t.Errorf(
				"twoSum(%v, %v) = %v, want %v", compactview.Ints(c.nums),
				c.target, got, c.want,
			)
		}
	}
}

func BenchmarkWorstCase(b *testing.B) {
	c := worstCase()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		runtime.KeepAlive(twoSum(c.nums, c.target))
	}
}

func worstCase() testCase {
	var size int = 1e5
	nums := make([]int, size)
	for i := range nums {
		nums[i] = i
	}
	return testCase{
		nums:   nums,
		target: (size - 2) + (size - 1),
		want:   []int{size - 2, size - 1},
	}
}
