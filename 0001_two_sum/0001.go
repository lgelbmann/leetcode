package p0001_two_sum

func twoSum(nums []int, target int) []int {
	indices := make(map[int32]int32, len(nums))
	for i, value := range nums {
		complement := int32(target - value)
		if otherIndex, ok := indices[complement]; ok {
			return []int{int(otherIndex), i}
		}
		indices[int32(value)] = int32(i)
	}
	panic("no solution exists")
}
